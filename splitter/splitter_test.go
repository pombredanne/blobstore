package splitter

import (
	"bytes"
	"io"
	"reflect"
	"strings"
	"testing"
)

func TestModHashSimple(t *testing.T) {
	// A trivial validation, make sure we get the expected results when the
	// base and modulus are round powers of two, so that the hash values will
	// match an exact suffix of the input bytes.
	h := NewModHasher(256, 1<<32, 8)
	tests := []struct {
		in, out byte
		want    uint
	}{
		{1, 0, 0x00000001},
		{2, 0, 0x00000102},
		{3, 0, 0x00010203},
		{4, 0, 0x01020304},
		{160, 0, 0x020304A0},
		{163, 0, 0x0304a0a3},
		{170, 0, 0x04a0a3aa},
		{15, 0, 0xa0a3aa0f},
		{16, 1, 0xa3aa0f10},
		{17, 2, 0xaa0f1011},
		{18, 3, 0x0f101112}, // match 1
		{15, 4, 0x1011120f},
		{16, 160, 0x11120f10},
		{17, 163, 0x120f1011},
		{18, 170, 0x0f101112}, // match 2
	}

	for _, test := range tests {
		got := h.Update(test.in, test.out)
		if got != test.want {
			t.Errorf("Update(%x, %x): got %x, want %x", test.in, test.out, got, test.want)
		}
	}
}

func TestModHashComplex(t *testing.T) {
	// Validation with "non-trivial" settings, to make sure we get the expected
	// results with roll-over.  These values were hand-verified.
	const (
		base = 7
		mod  = 257
		size = 5
	)
	input := []byte{
		0, 0, 0, 0, 0, // sentinels
		1, 3, 2, 8, 9,
		4, 7, 11, 75, 1,
		0, 1, 3, 2, 8, 9,
		15, 7,
	}

	h := NewModHasher(base, mod, size)

	for i, in := range input[size:] {
		data := input[i+1 : i+1+size]
		want := wantHash(base, mod, data)
		out := input[i]
		got := h.Update(in, out)
		if got != want {
			t.Errorf("Update(%d, %d): got %d, want %d", in, out, got, want)
		}
	}
}

const maxWindow = 8

func TestModHash(t *testing.T) {
	const (
		base = 2147483659
		mod  = 1031
	)
	for i := 1; i <= maxWindow; i++ {
		windowTest(t, NewModHasher(base, mod, i))
	}
}

func windowTest(t *testing.T, h Hasher) {
	// Make sure that we get the same hash value when the window has the same
	// contents.
	const keyValue = 22
	testData := make([]byte, h.Size())
	testData = append(testData, []byte{
		1, 2, 3, 4, 5, 6, 7, 8, 11, keyValue, 2, 3, 4, 5, 6, 7, 8, 11, 15, 17,
		33, 44, 55, 66, 77, 88, 3, 5, 7, 11, 13, 17, 19, 23, 3, 4, 5, 6, 7, 8,
		11, keyValue, 2, 3, 4, 5, 6, 7, 8, 11, keyValue, 24, 26, 28, 30,
	}...)

	var keyHash uint
	for i, in := range testData[h.Size():] {
		out := testData[i]
		v := h.Update(in, out)
		if in != keyValue {
			continue
		}
		if keyHash == 0 {
			keyHash = v
			t.Logf("At #%d, set hash for key value %d to %08x", i, keyValue, keyHash)
		} else if v != keyHash {
			t.Errorf("#%d: Update(%02x, %02x): got %d, want %d", i, in, out, v, keyHash)
		}
	}
}

// wantHash computes a raw mod-hash over the given slice without using sliding.
// This is used to check the outcome of a modHash that does slide.
func wantHash(base, mod int, data []byte) uint {
	var want int
	for _, v := range data {
		want = ((want * base) + int(v)) % mod
	}
	return uint(want)
}

// burstyReader implements io.Reader, returning chunks from r whose size is
// bounded above by the specified byte lengths, to simulate a reader that does
// not always deliver all that was requested.
type burstyReader struct {
	r   io.Reader
	len []int
	pos int
}

func (b *burstyReader) Read(buf []byte) (int, error) {
	cap := len(buf)
	if len(b.len) > b.pos {
		if n := b.len[b.pos]; n < cap {
			cap = b.len[b.pos]
		}
		b.pos = (b.pos + 1) % len(b.len)
	}
	return b.r.Read(buf[:cap])
}

func newBurstyReader(s string, sizes ...int) io.Reader {
	return &burstyReader{strings.NewReader(s), sizes, 0}
}

// dummyHash is a mock Hash implementation used for testing Splitter.  It
// returns a fixed value for all updates except a designated value.
type dummyHash struct {
	magic byte
	hash  uint
	size  int
}

func (d dummyHash) Update(in, out byte) uint {
	if in == d.magic {
		return 1
	}
	return d.hash
}

func (d dummyHash) Reset()    {}
func (d dummyHash) Size() int { return d.size }

func TestSplitterMin(t *testing.T) {
	const minBytes = 10
	var d = dummyHash{
		magic: '|',
		hash:  12345,
		size:  1,
	}
	s := New(strings.NewReader("abc|def|ghi|jkl|mno"), Hash(d), Min(minBytes))
	b, err := s.Next()
	if err != nil {
		t.Fatal(err)
	}
	if len(b) < minBytes {
		t.Errorf("len(b): got %d, want at least %d", len(b), minBytes)
	}
	t.Logf("b=%q", string(b))
}

func TestSplitterMax(t *testing.T) {
	const maxBytes = 10
	var d = dummyHash{
		hash: 12345,
		size: 1,
	}
	s := New(strings.NewReader("abc|def|ghi|jkl|mno"), Hash(d), Max(maxBytes))
	b, err := s.Next()
	if err != nil {
		t.Fatal(err)
	}
	if len(b) > maxBytes {
		t.Errorf("len(b): got %d, want at most %d", len(b), maxBytes)
	}
	t.Logf("b=%q", string(b))
}

func TestSplitterBlocks(t *testing.T) {
	var d = dummyHash{
		magic: '|',
		hash:  12345,
		size:  5,
	}
	tests := []struct {
		input    string
		min, max int
		blocks   []string
	}{
		{"", 5, 15, nil},
		{"abc", 5, 15, []string{"abc"}},
		{"|", 0, 15, []string{"|"}},
		{"x||y", 1, 15, []string{"x|", "|y"}},
		{"|||x", 0, 15, []string{"|", "|", "|x"}},
		{"a|bc|defg|hijklmno|pqrst", 2, 8, []string{"a|bc", "|defg", "|hijklmn", "o|pqrst"}},
		{"abcdefgh|ijklmnop|||q", 5, 100, []string{"abcdefgh", "|ijklmnop", "|||q"}},
		{"a|b|c|d|e|", 0, 100, []string{"a", "|b", "|c", "|d", "|e", "|"}},
		{"abcdefghijk", 4, 4, []string{"abcd", "efgh", "ijk"}},
	}

	for _, test := range tests {
		d.Reset()
		r := newBurstyReader(test.input, 3, 5, 1, 4, 17, 20)
		s := New(r, Hash(d), Min(test.min), Max(test.max))
		var bs []string
		if err := s.Split(func(b []byte) error {
			bs = append(bs, string(b))
			return nil
		}); err != nil {
			t.Fatal(err)
		}
		if !reflect.DeepEqual(bs, test.blocks) {
			t.Errorf("split %q: got %+q, want %+q", test.input, bs, test.blocks)
		}
	}
}

func TestDefaultHash(t *testing.T) {
	var numCalls int
	save := DefaultHasher
	DefaultHasher = func() Hasher {
		numCalls++
		return save()
	}

	var buf bytes.Buffer
	s1 := New(&buf)
	s2 := New(&buf)

	if numCalls != 2 {
		t.Errorf("DefaultHash calls: got %d, want %d", numCalls, 2)
	}
	if s1.hash != s2.hash {
		t.Logf("Got expected unequal hashes: %p, %p", s1.hash, s2.hash)
	} else {
		t.Errorf("Equal hashes for distinct splitters: %p, %p", s1.hash, s2.hash)
	}
}
