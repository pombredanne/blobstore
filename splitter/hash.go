package splitter

// This file provides the Hasher interface and a basic implementation for it.

// A Hasher implements a rolling hash function.
type Hasher interface {
	// Update updates the hash state by adding "in" and removing "out".  The
	// updated hash value is returned.
	Update(in, out byte) uint

	// Size returns the size of the rolling hash window in bytes.
	Size() int

	// Reset returns the hash to its initial state.
	Reset()
}

const (
	// DefaultMod is the default hash modulus.
	DefaultMod = 2147483659
	// DefaultBase is the default base multiplier.
	DefaultBase = 1031
	// DefaultWindow is the default window size, in bytes.
	DefaultWindow = 48
)

// DefaultHasher is used to create the default Hash used by a Splitter if it is
// not overridden by the UseHash option.  The built-in value returns a
// Rabin-Karp rolling hash with default parameters, but the value can be set to
// replace the default.
var DefaultHasher = func() Hasher {
	return NewModHasher(DefaultBase, DefaultMod, DefaultWindow)
}

type modHash struct {
	hash uint // Current hash state.
	base int  // Base for exponentiation.
	mod  int  // Modulus, should usually be prime.
	inv  int  // Base shifted by size-1 bytes, for subtraction.
	size int  // Window size, in bytes.
}

// NewModHasher returns a Hasher implementation that uses the Rabin-Karp
// rolling hash construction with the given base and modulus.
func NewModHasher(base, modulus, windowSize int) Hasher {
	return &modHash{
		base: base,
		mod:  modulus,
		inv:  exptmod(base, windowSize-1, modulus),
		size: windowSize,
	}
}

// Reset implements a required method of the Hasher interface.
func (m *modHash) Reset() { m.hash = 0 }

// Update implements a required method of the Hasher interface.
func (m *modHash) Update(in, out byte) uint {
	h := m.base*(int(m.hash)-m.inv*int(out)) + int(in)
	h %= m.mod
	if h < 0 {
		h += m.mod
	}
	m.hash = uint(h)
	return m.hash
}

// Size implements a required method of the Hasher interface.
func (m *modHash) Size() int { return m.size }

// exptmod(b, e, m) computes b**e modulo m.
func exptmod(b, e, m int) int {
	s := 1
	for e != 0 {
		if e&1 == 1 {
			s = (s * b) % m
		}
		b = (b * b) % m
		e >>= 1
	}
	return s
}
