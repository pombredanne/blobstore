// Package splitter implements a content-sensitive block splitter based on
// rolling hash functions.
//
// The algorithm used to split data into blocks is based on the one from LBFS:
//  http://pdos.csail.mit.edu/lbfs/
// As described in the SOSP 2001 paper "A Low-Bandwidth Network File System":
//  http://pdos.csail.mit.edu/lbfs/papers/sosp01/lbfs.pdf
//
// This package provides a rolling hash using the Rabin-Karp construction, but
// alternative implementations can be plugged in via the Hasher interface.
//
// Example:
//   s := splitter.New(block.Max(8192))
//   for {
//      data, err := s.Next()
//      if err == io.EOF {
//        break
//      } else if err != nil {
//        log.Fatalf("Failed to read a block: %v", err)
//      }
//      handleBlock(data) // data is only valid until a subsequent Next call
//   }
package splitter

import "io"

const (
	// defaultMin is the minimum block size in bytes that a Splitter will
	// generate if the Min option is not specified.
	defaultMin = 2048

	// defaultMax is the maximum block size in bytes that a Splitter will
	// generate if the Max option is not specified.
	defaultMax = 65536

	// defaultSize is the expected block size in bytes that a Splitter will
	// generate if the Size option is not specified.
	defaultSize = 8192
)

// An Option is used to configure the behaviour of a Splitter.
type Option func(*Splitter)

// MinB returns an Option that sets the minimum number of bytes in a block
// produced by the Splitter.
func Min(min int) Option { return func(s *Splitter) { s.min = min } }

// Max returns an Option that sets the maximum number of bytes in a block
// produced by the Splitter.
func Max(max int) Option { return func(s *Splitter) { s.max = max } }

// Size returns an Option that sets the expected number of bytes in a block
// produced by the Splitter.  The expectation is based on an assumption of
// random data, so the actual size will be bounded by Min and Max.
func Size(exp int) Option { return func(s *Splitter) { s.exp = exp } }

// Hash returns an Option that sets the Hasher implementation used by a Splitter
// to find block boundaries.
func Hash(h Hasher) Option { return func(s *Splitter) { s.hash = h } }

// A Splitter wraps an underlying io.Reader to split the data from the reader
// into blocks using a rolling hash.
type Splitter struct {
	reader io.Reader // The underlying source of block data.

	hash     Hasher // The rolling hash used to find breakpoints.
	min, max int    // Minimum and maximum block sizes in bytes.
	exp      int    // Expected block size in bytes.
	next     int    // Next unused offset in buf.
	end      int    // End of previous block.
	buf      []byte // Incoming data buffer.
}

// New constructs a new Splitter that reads its data from rd and partitions it
// into blocks using the rolling hash h to find split points.
//
// Options:
//   Min(n)   -- return no block with fewer than n bytes except at EOF.
//   Max(n)   -- return no block with more than n bytes.
//   Size(n)  -- try to make the average block size approach n bytes.
//   Hash(h)  -- use h as the rolling hash for splitting blocks.
//
// If no constraints on the block size are given, sensible defaults are used.
func New(rd io.Reader, opts ...Option) *Splitter {
	s := &Splitter{
		hash:   DefaultHasher(),
		reader: rd,
		min:    defaultMin,
		max:    defaultMax,
		exp:    defaultSize,
	}
	for _, opt := range opts {
		opt(s)
	}
	size := s.hash.Size()
	s.buf = make([]byte, s.max+size)
	s.next = size
	s.end = size
	return s
}

// Next returns the next available block, or an error.  The slice returned is
// only valid until a subsequent call of Next.  Returns nil, io.EOF when no
// further blocks are available.
func (s *Splitter) Next() ([]byte, error) {
	wSize := s.hash.Size()

	// Shift out the previous block so that end is at the first position after
	// the window.  This invalidates any previous slice that was returned from
	// this buffer, since the data have now moved.
	if s.end > wSize {
		diff := s.end - wSize
		copy(s.buf, s.buf[diff:])
		s.end = wSize
		s.next -= diff
	}

	i := s.end // The position of the next potential block boundary
	for {
		// Try to read more data into the buffer.  An EOF at this point is not
		// an error, since there may be data left in the buffer from earlier.
		nr, err := s.reader.Read(s.buf[s.next:])
		if err != nil && err != io.EOF {
			return nil, err
		}
		s.next += nr

		// Look for a block boundary: A point where the hash value goes to 1
		// modulo the desired block size, or we run out of buffered data.
		isCut := false
		for ; i < s.next; i++ {
			in, out := s.buf[i], s.buf[i-wSize]
			u := s.hash.Update(in, out)
			isCut = u%uint(s.exp) == 1 && i-s.end > s.min
			if isCut {
				break
			}
		}

		// If we found a block cut, or have reached the maximum block size, or
		// there is no input left, update state and return the block.
		if isCut || i >= len(s.buf) || (i > s.end && err == io.EOF) {
			block := s.buf[s.end:i]
			s.end = i
			return block, nil
		}

		// We didn't find a cut, and there's room for more data in the buffer.
		// If there's still something left to read, go back for another chunk.
		if err == io.EOF {
			break
		}
	}
	// No more blocks available, end of input.
	return nil, io.EOF
}

// Split splits blocks from s and passes each block in sequence to f, until
// there are no further blocks or until f returns an error.  If f returns a
// non-nil error besides io.EOF, that error is passed back to the caller of
// Split.
//
// At the end of the input, or if f returns io.EOF, processing stops and split
// returns nil.  The slice passed to f is only valid while f is active; the
// splitter may re-use the underlying array for a subsequent call.
func (s *Splitter) Split(f func(data []byte) error) error {
	for {
		block, err := s.Next()
		if err == io.EOF {
			return nil
		} else if err != nil {
			return err
		}
		if err := f(block); err != nil {
			if err == io.EOF {
				return nil
			}
			return err
		}
	}
	panic("unreachable")
}

/*
 Implementation notes:

 The Splitter maintains a buffer big enough to hold a full maximum-length block
 of data, plus enough extra for the hash window.  The buffer is organized as
 follows:

    0                                                           len(buf)
   |xxxxxxxxxxabcdefghijklmnopqrs------------------------------|
    windowSize^       ^end       ^next

 All the bytes prior to end are part of the previous block, and the latest
 known block boundary is initially at windowSize.  If end > windowSize, the
 first step is to shift out the previous block so that end = windowSize:

   |xxabcdefghijklmnopqrs--------------------------------------|
              ^end       ^next

 Next, if next < len(buf), try to fill the buffer with new data:

   |xxabcdefghijklmnopqrsAAAAAAAAAAAAAAAAAAAAAAAAAAA-----------|
              ^end                                  ^next

 Now we scan forward from i = end until we reach next or find a block boundary.
 For a position to count as a block boundary, it must be on a hash cut, and
 must be at least minBytes greater than end.

   |xxabcdefghijklmnopqrsAAAAAAAAAAAAAAAAAAAAAAAAAAA-----------|
              ^end                 ^i               ^next

 There are now three cases to consider:
  (a) i is at a hash cut at least min greater than end.
      This is a normal block, which we must return.
  (b) i == len(buf).
      This is a long block, capped by the max block size, which we must return.
  (c) i > end and input is at EOF.
      This is a non-empty tail block, which we must return.

 If none of these cases apply, it means we have not seen a block boundary and
 have space left in the buffer. If the input is not exhausted, we go back and
 try to read another chunk from the input; otherwise we report EOF.

 If we do have a block to return, its data are in buf[end:i]. When we do so, we
 also update end to i, to mark the end of the block for the next call.

             [*********************]<< returned block
   |xxabcdefghijklmnopqrsAAAAAAAAAAAAAAAAAAAAAAAAAAA-----------|
                                   ^end             ^next

 At this point, the buffer is in a clean state for the next iteration.
*/
