// Binary treetool implements a simple Merkle-tree filesystem on top of a
// blobstore.  There are two basic operations: Writing copies a tree of files
// from the filesystem into a blobstore; reading copies a tree of files from
// the blobstore to the filesystem.  The tool also supports a few other read
// based operations (e.g., listing, garbage collection), path lookup.
//
// Writing:
//   treetool -write -store mystore path1 path2 path3 ...
//
// When writing is complete, each root blob ID is printed to stdout, and if the
// -root flag is set, also written to a file at that path.
//
// Reading:
//  treetool -store blobstore -root output -read $BLOBID
//
// Output is populated into the specified output directory, which is created if
// necessary.
//
// Add the -v flag for verbose output.
package main

import (
	"bytes"
	"crypto/sha1"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/user"
	"path/filepath"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"syscall"
	"time"

	"bitbucket.org/creachadair/blobstore/cmd/flags"
	"bitbucket.org/creachadair/blobstore/splitter"
	"bitbucket.org/creachadair/blobstore/store"
	"bitbucket.org/creachadair/blobstore/store/reader"
	"bitbucket.org/creachadair/blobstore/store/storeutil"
	"bitbucket.org/creachadair/blobstore/tree"
	"bitbucket.org/creachadair/group"
	"bitbucket.org/creachadair/stringset"
)

var (
	storePath = flag.String("store", "", "Blobstore path (required)")
	rootPath  = flag.String("root", "", "Output root at this path")
	doFind    = flag.String("find", "", "Find paths by regexp match")
	doGC      = flag.Bool("gc", false, "Delete unused blobs")
	doList    = flag.Bool("ls", false, "List the contents of the store")
	doRead    = flag.String("read", "", "Read out the a subtree of the store to this directory")
	doWrite   = flag.Bool("write", false, "Write trees into the store")
	doVerbose = flag.Bool("v", false, "Be verbose (enables logging)")
	noSplit   = flag.Bool("nosplit", false, "Disable splitting of file blobs")

	g = group.New()
)

func main() {
	flag.Parse()

	if *storePath == "" {
		log.Fatal("You must provide a non-empty --store path")
	}
	fs, err := flags.Storage(*storePath)
	if err != nil {
		log.Fatalf("Unable to %s", err)
	}
	st := store.New(fs, sha1.New, store.Compress(storeutil.ZLib))

	// Copy a tree of files into the blob store, recursively.
	if *doWrite {
		var roots []string
		ids := make(chan string)
		collect := group.New()
		collect.Go(func() error {
			for id := range ids {
				roots = append(roots, id)
			}
			return nil
		})

		for _, path := range flag.Args() {
			path := path
			g.Go(func() error {
				id, err := makeTree(st, path)
				if err == nil {
					ids <- id + "\t" + path
				}
				return err
			})
		}
		err := g.Wait()
		close(ids)
		collect.Wait()
		if err != nil {
			log.Fatalf("Copying failed: %v", err)
		}
		sort.Strings(roots)
		printRoot(strings.Join(roots, "\n"))
		return
	}

	// List the contents of a blob store, based on the root path.
	if *doList {
		for _, id := range flag.Args() {
			if err := listTree(st, "", id); err != nil {
				log.Fatalf("Error listing %q: %v", id, err)
			}
		}
		return
	}

	// Garbage collect the blob store, based on the roots given.
	if *doGC {
		if flag.NArg() == 0 {
			log.Fatal("You must provide at least one root id to use --gc")
		}
		inUse := make(stringset.Set)
		for _, id := range flag.Args() {
			if err := markBlobs(st, id, inUse); err != nil {
				log.Fatalf("Error marking %q: %v", id, err)
			}
		}
		vlog("Marked %d blobs", inUse.Len())
		toKill := stringset.New()
		err := st.Storage().Keys(func(id string) error {
			if !inUse.Contains(id) {
				toKill.Add(id)
			}
			return nil
		})
		if err != nil {
			log.Fatalf("Error enumerating blobs: %v", err)
		}
		vlog("Found %d unused blobs", toKill.Len())
		for _, id := range toKill.Keys() {
			if err := st.Storage().Remove(id); err != nil {
				log.Printf("Unable to remove %q: %v", id, err)
			} else {
				vlog("D: %s", id)
			}
		}
		return
	}

	if *doFind != "" {
		if flag.NArg() == 0 {
			log.Fatal("You must provide at least one root id to use --find")
		}
		r, err := regexp.Compile(*doFind)
		if err != nil {
			log.Fatalf("Unable to compile %q: %v", *doFind, err)
		}
		hits := stringset.New()
		for _, id := range flag.Args() {
			if err := walkTree(st, "", id, func(path string, t *tree.Node, fi *fileInfo) error {
				if r.MatchString(path) {
					hits.Add(fmt.Sprintf("%s\t%s", t.ID, path))
				}
				return nil
			}); err != nil {
				log.Fatalf("Error resolving %q: %v", *doFind, err)
			}
		}
		fmt.Println(strings.Join(hits.Keys(), "\n"))
		return
	}

	if *doRead != "" {
		// Copy the contents of the blob store to someplace else.
		if err := os.MkdirAll(*doRead, 0755); err != nil {
			log.Fatalf("Unable to create output directory: %v", err)
		}
		for _, id := range flag.Args() {
			vlog("U: %s", id)
			if err := unpackTree(st, *doRead, id); err != nil {
				log.Fatalf("Error unpacking %q: %v", id, err)
			}
		}
		return
	}

	log.Fatal("You must provide a command; use --help for options")
}

func printRoot(id string) {
	if *rootPath != "" {
		if err := ioutil.WriteFile(*rootPath, []byte(id+"\n"), 0644); err != nil {
			log.Fatalf("Failed to write root: %v", err)
		}
		vlog("Wrote root to %q", *rootPath)
	}
	fmt.Println(id)
}

func vlog(msg string, args ...interface{}) {
	if *doVerbose {
		log.Printf(msg, args...)
	}
}

func setAttrs(t *tree.Node, fi *fileInfo, path string, err error) error {
	if err == nil {
		// Attempt to set the modification time, if defined.
		if !fi.Mtime.IsZero() {
			os.Chtimes(path, fi.Mtime, fi.Mtime)
		}
		// TODO(fromberger): Do something with the owner name.
		// TODO(fromberger): Do something sensible with extended attributes.
	}
	return err
}

func markBlobs(st *store.Store, id string, marked stringset.Set) error {
	return walkTree(st, ".", id, func(_ string, t *tree.Node, fi *fileInfo) error {
		marked.Add(t.ID)
		marked.Add(t.Kids...)
		vlog("M: %s (+%d kids)", t.ID, len(t.Kids))
		return nil
	})
}

// walkTree behaves like filepath.Walk, but on a blobstore tree.
func walkTree(st *store.Store, base, id string, visit func(string, *tree.Node, *fileInfo) error) error {
	t, fi, err := loadNode(st, id)
	if err != nil {
		return err
	}
	path := filepath.Join(base, fi.Name)
	err = visit(path, t, fi)
	if err == filepath.SkipDir && fi.Mode.IsDir() {
		return nil // skip children, but not an error
	} else if err != nil {
		return err
	}
	if fi.Mode.IsDir() {
		for _, id := range t.Kids {
			if err := walkTree(st, path, id, visit); err != nil {
				return err
			}
		}
	}
	return nil
}

func listTree(st *store.Store, base, id string) error {
	return walkTree(st, base, id, func(path string, t *tree.Node, fi *fileInfo) error {
		fmt.Println(fi.pretty(path, len(t.Kids)))
		if *doVerbose {
			fmt.Printf("\t@ %s\n", t.ID)
		}
		if fi.Mode&os.ModeSymlink != 0 {
			raw, err := reader.ReadAll(st, t.Kids)
			if err != nil {
				return err
			}
			fmt.Println("\t⇒", string(raw))
		}
		return nil
	})
}

func loadNode(st *store.Store, id string) (*tree.Node, *fileInfo, error) {
	t, err := tree.Open(st, id)
	if err != nil {
		return nil, nil, err
	}
	defer t.Close()

	dec := json.NewDecoder(t)
	var fi fileInfo
	if err := dec.Decode(&fi); err != nil {
		return nil, nil, err
	}
	return t, &fi, nil
}

func unpackTree(st *store.Store, root, id string) error {
	return walkTree(st, root, id, func(path string, t *tree.Node, fi *fileInfo) error {
		// Directory
		if fi.Mode.IsDir() {
			defer vlog("D: %s", path)
			if err := os.MkdirAll(path, fi.Mode); err != nil {
				return err
			}
			return setAttrs(t, fi, path, nil)
		}

		// Symlink
		if fi.Mode&os.ModeSymlink != 0 {
			raw, err := reader.ReadAll(st, t.Kids)
			if err != nil {
				return err
			}
			target := string(raw)
			vlog("L: %s ⇒ %s", path, target)
			return os.Symlink(target, path)
		}

		// Regular file (as far as we're concerned).
		f, err := os.OpenFile(path, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, fi.Mode)
		if err != nil {
			return err
		}
		r := reader.Concat(st, t.Kids)
		defer r.Close()
		nr, err := io.Copy(f, r)
		vlog("F: %s (%d blocks, %d bytes)", path, len(t.Kids), nr)
		return setAttrs(t, fi, path, f.Close())
	})
}

// makeTree stuffs a filesystem tree based at root into the blobstore st.
func makeTree(st *store.Store, root string) (string, error) {
	fi, err := os.Lstat(root)
	if err != nil {
		return "", err
	}
	switch m := fi.Mode(); {
	case m.IsDir():
		return dirNode(st, fi, root)
	case m&os.ModeSymlink != 0:
		return linkNode(st, fi, root)
	case m.IsRegular():
		return fileNode(st, fi, root)
	default:
		return newNode(st, root, fi, nil)
	}
}

// newNode creates a treenode representing the given root, and returns its id.
// Filesystem attributes are stored as metadata.
func newNode(st *store.Store, root string, fi os.FileInfo, kids []string) (string, error) {
	info := fileInfo{
		Name:  fi.Name(),
		Size:  fi.Size(),
		Mode:  fi.Mode(),
		Mtime: fi.ModTime(),
	}

	// Where possible, fetch the user who owns the file.
	if s, ok := fi.Sys().(*syscall.Stat_t); ok && s != nil {
		u, err := user.LookupId(strconv.Itoa(int(s.Uid)))
		if err == nil {
			info.User = u.Username
		}
	}

	bits, err := json.Marshal(&info)
	if err != nil {
		return "", err
	}
	return tree.New(st, kids, bytes.NewReader(bits))
}

// dirNode recursively creates a treenode for a directory.
func dirNode(st *store.Store, fi os.FileInfo, root string) (string, error) {
	f, err := os.Open(root)
	if err != nil {
		return "", err
	}
	names, err := f.Readdirnames(-1)
	f.Close()
	if err != nil {
		return "", err
	}
	sort.Strings(names)

	vlog("D: %s (%d entries)", root, len(names))
	kch := make(chan struct{})
	g.Go(func() error {
		defer close(kch)
		for i, kid := range names {
			id, err := makeTree(st, filepath.Join(root, kid))
			if err != nil {
				return err
			}
			names[i] = id
		}
		return nil
	})
	<-kch
	return newNode(st, root, fi, names)
}

// linkNode creates a treenode for a symlink. The contents of the link will be
// stored in a single bare blob recording its target.
func linkNode(st *store.Store, fi os.FileInfo, root string) (string, error) {
	target, err := os.Readlink(root)
	if err != nil {
		return "", err
	}
	vlog("L: %s -> %s", root, target)
	id, err := st.Copy(strings.NewReader(target))
	if err != nil {
		return "", err
	}
	return newNode(st, root, fi, []string{id})
}

// fileNode creates a treenode for a (regular) file. The file will be split
// using a rolling hash and the kids will be bare blobs containing those
// blocks.
func fileNode(st *store.Store, fi os.FileInfo, root string) (string, error) {
	f, err := os.Open(root)
	if err != nil {
		return "", err
	}
	defer f.Close()

	start := time.Now()
	var ids []string
	if *noSplit {
		id, err := st.Copy(f)
		if err != nil {
			return "", err
		}
		ids = append(ids, id)
	} else if err := splitter.New(f).Split(func(data []byte) error {
		id, err := st.Copy(bytes.NewReader(data))
		ids = append(ids, id)
		return err
	}); err != nil {
		return "", err
	}
	vlog("F: %s (%d blocks, %d bytes, %v)", root, len(ids), fi.Size(), time.Since(start))
	return newNode(st, root, fi, ids)
}

// fileInfo encodes file metadata as JSON.
type fileInfo struct {
	Name  string      `json:"name"`
	Size  int64       `json:"size"`
	Mode  os.FileMode `json:"mode"`
	Mtime time.Time   `json:"mtime,omitempty"`
	User  string      `json:"user,omitempty"`
}

func (fi fileInfo) pretty(path string, nlinks int) string {
	user := fi.User
	if user == "" {
		user = "<unknown>"
	}
	return fmt.Sprintf("%s %2d %10s %8d %s %s", fi.Mode.String(), nlinks, user, fi.Size, fi.Mtime.Format(time.Stamp), path)
}
