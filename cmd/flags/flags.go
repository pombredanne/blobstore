// Package flags handles flag parsing for the command-line tools.
package flags

import (
	"fmt"
	"strings"

	"bitbucket.org/creachadair/blobstore/store/s3storage"
	"bitbucket.org/creachadair/blobstore/store/storage"

	"gopkg.in/amz.v2/aws"
	"gopkg.in/amz.v2/s3"
)

// Storage parses a store spec and returns a storage interface.  If path is the
// literal string ":memory:", an in-memory store is created.  If path begins
// with "s3:" it is treated as an S3 bucket spec.  Otherwise, path is treated
// as a local directory path.
func Storage(path string) (storage.Interface, error) {
	if path == ":memory:" {
		return storage.Memory(), nil
	} else if amzn := strings.TrimPrefix(path, "s3:"); amzn != path {
		parts := strings.SplitN(amzn, ":", 3)
		if len(parts) < 2 || len(parts) > 3 {
			return nil, fmt.Errorf("parse S3 address %q: want <bucket>:<region>[:<prefix>]", amzn)
		}
		var prefix string
		bucket, region := parts[0], parts[1]
		if len(parts) == 3 {
			prefix = parts[2]
		}
		auth, err := aws.EnvAuth()
		if err != nil {
			return nil, fmt.Errorf("get AWS credentials: %v", err)
		}
		rgn, ok := aws.Regions[region]
		if !ok {
			return nil, fmt.Errorf("resolve region %q", region)
		}
		return s3storage.Bucket(s3.New(auth, rgn), bucket, prefix), nil
	}
	fs, err := storage.Local(path, 0755)
	if err != nil {
		return nil, fmt.Errorf("open local storage at %q: %v", path, err)
	}
	return fs, err
}
