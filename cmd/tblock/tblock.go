// Binary tblock is a test driver for the splitter package.  It permits you to
// apply the splitter to arbitrary input files named on the command line, and
// to control various parameters of its rolling hash function.
//
// Usage:
//   tblock [options] filepath...
//
// Use "tblock -help" for flags.  The important flags are:
//   -min n    -- minimum block size in bytes.
//   -max n    -- maximum block size in bytes.
//   -window n -- sliding hash window size in bytes.
//
package main

import (
	"crypto/sha1"
	"encoding/hex"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"

	"bitbucket.org/creachadair/blobstore/splitter"
	"bitbucket.org/creachadair/goflags/stringlist"
)

var (
	minSize    = flag.Int("min", 2048, "Minimum block size (bytes)")
	maxSize    = flag.Int("max", 65536, "Maximum block size (bytes)")
	expSize    = flag.Int("size", 8192, "Average block size (bytes)")
	base       = flag.Int("base", splitter.DefaultBase, "Hash base")
	modulus    = flag.Int("modulus", splitter.DefaultMod, "Hash modulus")
	windowSize = flag.Int("window", splitter.DefaultWindow, "Window size in bytes")
	pickBlocks = stringlist.Flag(stringlist.Trim(true))
)

func init() {
	flag.Var(pickBlocks, "pick", "Block numbers or ranges to write to stdout (comma-separated)")
	flag.Usage = func() {
		fmt.Fprintln(os.Stderr, `Usage: tblock [options] filepath...

Splits each of the named files into content-sensitive blocks using a rolling
hash function.  The size and digest of each block are printed to stdout.  If no
filepaths are given, or the filepath is "-", the tool reads from stdin.

Options:`)
		flag.PrintDefaults()
	}
}

func mustAtoi(s string) int {
	v, err := strconv.Atoi(s)
	if err != nil {
		panic(err)
	}
	return v
}

type ival struct{ lo, hi int }

func (v ival) String() string {
	s := strconv.Itoa(v.lo)
	if v.hi == v.lo+1 {
		return s
	}
	return s + ".." + strconv.Itoa(v.hi)
}

type ivals []ival

func (iv ivals) Len() int           { return len(iv) }
func (iv ivals) Swap(i, j int)      { iv[i], iv[j] = iv[j], iv[i] }
func (iv ivals) Less(i, j int) bool { return iv[i].lo < iv[j].lo }

func (iv ivals) Contains(i int) bool {
	for _, v := range iv {
		if v.lo > i {
			return false
		} else if v.hi > i {
			return true
		}
	}
	return false
}

func stderr(msg string, args ...interface{}) { fmt.Fprintf(os.Stderr, msg, args...) }

func (iv ivals) String() string {
	ss := make([]string, len(iv))
	for i, v := range iv {
		ss[i] = v.String()
	}
	return "{" + strings.Join(ss, ", ") + "}"
}

func pickMap() ivals {
	var iv ivals
	for _, r := range pickBlocks.Strings() {
		ps := strings.SplitN(r, "-", 2)
		lo := mustAtoi(ps[0])
		if len(ps) == 2 {
			iv = append(iv, ival{lo, mustAtoi(ps[1])})
		} else {
			iv = append(iv, ival{lo, lo + 1})
		}
	}
	sort.Sort(iv)
	return iv
}

func main() {
	flag.Parse()

	var totalSize, totalBlocks, maxBlock int
	var minBlock = -1
	hash := splitter.NewModHasher(*base, *modulus, *windowSize)
	seen := make(map[string]int)
	opts := []splitter.Option{
		splitter.Min(*minSize),
		splitter.Max(*maxSize),
		splitter.Size(*expSize),
		splitter.Hash(hash),
	}

	paths := flag.Args()
	if len(paths) == 0 {
		paths = []string{"-"}
	}

	pick := pickMap()
	if len(pick) != 0 {
		stderr("Pick %v\n", pick)
	}
	for _, path := range paths {
		var in io.ReadCloser
		if path == "-" {
			in = os.Stdin
		} else if f, err := os.Open(path); err != nil {
			log.Printf("Unable to open %q: %v", path, err)
			continue
		} else {
			in = f
			fmt.Println("--", path)
		}
		hash.Reset()

		pos := 0
		nb := 0
		h := sha1.New()
		r := splitter.New(in, opts...)
		for {
			b, err := r.Next()
			if err != nil {
				log.Println(err)
				break
			} else if b == nil {
				break
			}

			nb++
			totalBlocks++
			h.Reset()
			h.Write(b) // no error
			sig := hex.EncodeToString(h.Sum(nil))
			seen[sig]++

			picked := pick.Contains(nb)
			size := len(b)
			c := ' '
			if picked {
				c = '!'
			} else if size <= *minSize {
				c = '<'
			} else if size == *maxSize {
				c = '>'
			}

			stderr("#%d\t%d\t%d\t%s [%d]%c\n", nb, pos, size, sig, seen[sig], c)
			pos += size
			totalSize += size
			if size < minBlock || minBlock < 0 {
				minBlock = size
			} else if size > maxBlock {
				maxBlock = size
			}

			if picked {
				os.Stdout.Write(b)
			}
		}
		stderr("%d\t[end]\t%d blocks\n", pos, nb)
		in.Close()
	}

	stderr(`Total blocks: %d
Total bytes:  %d
Minimum size: %d
Maximum size: %d
`, totalBlocks, totalSize, minBlock, maxBlock)
	stderr("Average size: %.1f\n", float64(totalSize)/float64(totalBlocks))
}
