// Binary blobstore provides a simple command-line interface to create and
// manipulate blob stores.  This is mainly intended for testing purposes.
//
// Usage:
//   blobstore [options] <args>...
package main

import (
	"bytes"
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"flag"
	"fmt"
	"hash"
	"io"
	"io/ioutil"
	"log"
	"os"
	"regexp"
	"strings"
	"sync"

	"bitbucket.org/creachadair/blobstore/cmd/flags"
	"bitbucket.org/creachadair/blobstore/splitter"
	"bitbucket.org/creachadair/blobstore/store"
	"bitbucket.org/creachadair/blobstore/store/storeutil"
	"bitbucket.org/creachadair/goflags/enum"
	"bitbucket.org/creachadair/group"
)

var (
	// Select the operation. These are mutually exclusive.
	doRead  = flag.Bool("read", false, "Read blobs from the store")
	doWrite = flag.Bool("write", false, "Write blobs into the store")
	doList  = flag.Bool("list", false, "List blob IDs known to the store")
	doDel   = flag.Bool("del", false, "Delete blobs from the store")

	// Other settings.
	doLit   = flag.Bool("args", false, "Use arguments as literal blob data or IDs")
	doCat   = flag.Bool("cat", false, "Concatenate blobs")
	doSplit = flag.Bool("split", false, "Split input blobs into blocks")

	// Store options
	storePath    = flag.String("store", os.Getenv("BLOBSTORE_PATH"), "Path of blobstore directory")
	uncompressed = flag.Bool("uncompressed", false, "Disable compression for the store")
	digestName   = enum.Flag("sha1", "sha256", "sha512", "md5")

	// Splitter options.
	splitMin  = flag.Int("splitmin", 2048, "Minimum split block size, in bytes")
	splitMax  = flag.Int("splitmax", 65536, "Maximum split block size, in bytes")
	splitSize = flag.Int("splitsize", 8192, "Desired split block size, in bytes")

	maxWorkers = flag.Int("workers", 32, "Maximum number of concurrent workers")
	match      = flag.String("match", "", "Select blobs whose contents match this regular expression")
	negate     = flag.Bool("not", false, "Negate the sense of --match")
	zero       = flag.Bool("zero", false, "Use NUL as an output separator instead of LF")

	matchRE *regexp.Regexp
	EOR     = "\n"
)

func init() {
	flag.Var(digestName, "digest", digestName.Help("The digest algorithm to use"))

	flag.Usage = func() {
		fmt.Fprintln(os.Stderr, `Usage: blobstore [options] [<store-path>] <args>...

Manipulate the contents of a content-addressable blob store.  If BLOBSTORE_PATH
is set in the environment, it will be used as the path for the store; otherwise
the store must be provided via the -store flag or the first argument.

If the -store has the form "s3:bucket:region", an S3 bucket will be used.
Set the AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY environment variables with
the credentials for the bucket.

To write blobs into the store:  blobstore -write ...
To read blobs from the store:   blobstore -read ...
To list blobs known blob IDs:   blobstore -list ...
To delete blobs from the store: blobstore -del ...

Blobs are identified by a hex digest.  Use "all" to denote all known blobs.

Options:`)
		flag.PrintDefaults()
	}
}

func fail(msg string, args ...interface{}) { log.Fatalf(msg, args...) }

func digest() func() hash.Hash {
	switch digestName.Key() {
	case "sha256":
		return sha256.New
	case "sha1":
		return sha1.New
	case "sha512":
		return sha512.New
	case "md5":
		return md5.New
	default:
		return nil
	}
}

func main() {
	flag.Parse()
	args := flag.Args()

	if numTrue(*doDel, *doList, *doRead, *doWrite) != 1 {
		fail("Exactly one of --del, --list, --read, or --write must be set")
	}
	if *storePath == "" {
		if len(args) == 0 {
			fail("The store path must not be empty")
		}
		*storePath = args[0]
		args = args[1:]
	}
	if r, err := regexp.Compile(*match); err != nil {
		fail("Invalid --match regexp %q: %v", *match, err)
	} else if *match != "" {
		matchRE = r
	}
	if *zero {
		EOR = "\x00"
	}

	fs, err := flags.Storage(*storePath)
	if err != nil {
		fail("Unable to %s", err)
	}
	st := store.New(fs, digest(), store.Compress(storeutil.ZLib))
	if *uncompressed {
		store.Compress(nil)(st)
	}

	switch {
	case *doDel:
		if len(args) == 1 && args[0] == "all" {
			args = mustListBlobs(st)
		}
		if err := deleteBlobs(st, args); err != nil {
			fail("Error deleting blobs: %v", err)
		}

	case *doWrite:
		names, rc := argsToReaders(args)
		if *doCat {
			names = nil
			var rs []io.Reader
			for _, r := range rc {
				rs = append(rs, r)
			}
			rc = []io.ReadCloser{ioutil.NopCloser(io.MultiReader(rs...))}
		}

		g := group.New()
		start := group.Capacity(g, *maxWorkers)
		var μ sync.Mutex
		for i, r := range rc {
			i, r := i, r
			start(func() error {
				defer r.Close()
				ids, err := writeBlob(st, r)
				if err != nil {
					return fmt.Errorf("writing blob: %v", err)
				}
				μ.Lock()
				printSummary(i, names, ids)
				μ.Unlock()
				return nil
			})
		}
		if err := g.Wait(); err != nil {
			log.Printf("Error: %v", err)
		}
	case *doList:
		if err := st.Storage().Keys(func(id string) error {
			if blobMatch(st, id, true) {
				fmt.Print(id, EOR)
			}
			return nil
		}); err != nil {
			log.Printf("Error listing blobs: %v", err)
		}

	case *doRead:
		if len(args) == 1 && args[0] == "all" {
			args = mustListBlobs(st)
		}
		if err := readBlobs(st, args, os.Stdout); err != nil {
			fail("Error reading blobs: %v", err)
		}
	}
}

func printSummary(i int, names, ids []string) {
	if len(ids) == 1 {
		fmt.Print(ids[0])
		if i < len(names) {
			fmt.Print("\t", names[i])
		}
	} else {
		if i < len(names) {
			fmt.Print("# ", names[i], EOR)
		}
		fmt.Print(strings.Join(ids, EOR))
	}
	fmt.Print(EOR)
}

func numTrue(bs ...bool) (n int) {
	for _, b := range bs {
		if b {
			n++
		}
	}
	return n
}

// writeBlob stores the contents of r as one or more blobs into s, respecting
// the split flag settings. Returns the IDs of the blobs created.
func writeBlob(s *store.Store, r io.Reader) ([]string, error) {
	if !*doSplit {
		id, err := s.Copy(r)
		if err != nil {
			return nil, err
		}
		return []string{id}, err
	}
	var ids []string
	sp := splitter.New(r, splitter.Min(*splitMin), splitter.Max(*splitMax), splitter.Size(*splitSize))
	err := sp.Split(func(blob []byte) error {
		id, err := s.Copy(bytes.NewReader(blob))
		ids = append(ids, id)
		return err
	})
	return ids, err
}

// mustListBlobs returns the IDs of all known blobs in st, or exits the program.
func mustListBlobs(st *store.Store) []string {
	var ids []string
	if err := st.Storage().Keys(func(id string) error {
		ids = append(ids, id)
		return nil
	}); err != nil {
		fail("Unable to list blobs for %v: %v", st, err)
	}
	return ids
}

// argsToReaders returns parallel slices of names and readers for the given
// arguments, respecting the input flag settings.
func argsToReaders(args []string) ([]string, []io.ReadCloser) {
	if len(args) == 0 {
		return nil, []io.ReadCloser{os.Stdin}
	}
	var names []string
	var rc []io.ReadCloser
	for _, arg := range args {
		if *doLit {
			rc = append(rc, ioutil.NopCloser(strings.NewReader(arg)))
		} else if f, err := os.Open(arg); err != nil {
			log.Printf("Error opening input: %v", err)
		} else {
			names = append(names, arg)
			rc = append(rc, f)
		}
	}
	return names, rc
}

// deleteBlobs removes the specified blob IDs from the store.  Not safe for
// concurrent access.
func deleteBlobs(st *store.Store, args []string) error {
	for _, id := range args {
		if !blobMatch(st, id, false) {
			continue
		}
		if err := st.Storage().Remove(id); err != nil {
			return fmt.Errorf("remove %q: %v", id, err)
		}
		fmt.Print(id, EOR)
	}
	return nil
}

// readBlobs sequentially reads the specified blobs from st and writes their
// contents to out.
func readBlobs(st *store.Store, ids []string, out io.Writer) error {
	for _, id := range ids {
		b, err := st.Open(id)
		if err != nil {
			return fmt.Errorf("opening blob %q: %v", id, err)
		}
		_, err = io.Copy(out, b)
		b.Close()
		if err != nil {
			return fmt.Errorf("copying blob %q: %v", id, err)
		}
	}
	return nil
}

// blobMatch reports whether the blob with the given ID is matched by the
// current match flag settings.  In case of an error reading the blob contents,
// onErr is returned.
func blobMatch(st *store.Store, id string, onErr bool) bool {
	if matchRE != nil {
		v, err := st.ReadAll(id)
		if err != nil {
			log.Printf("Error reading %q: %v", id, err)
			return onErr
		}
		return matchRE.Match(v) != *negate
	}
	return true
}
