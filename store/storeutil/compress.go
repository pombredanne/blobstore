// Package storeutil provides some helper functions for the store package.
package storeutil

import (
	"compress/zlib"
	"io"
)

// ZLib satisfies the store.Compressor interface using the compress/zlib package.
var ZLib = zlibCompressor{}

type zlibCompressor struct{}

func (zlibCompressor) Compress(w io.Writer) (io.WriteCloser, error)  { return zlib.NewWriter(w), nil }
func (zlibCompressor) Uncompress(r io.Reader) (io.ReadCloser, error) { return zlib.NewReader(r) }
