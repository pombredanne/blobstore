// Package reader provides an io.ReadCloser implementation for a sequence of
// blobs in a blobstore.
//
// Example:
//   s, err := store.New(...)
//   ...
//   r := reader.Concat(s, ids)
//   if _, err := io.Copy(os.Stdout, r); err != nil {
//     log.Fatal(err)
//   }
//   r.Close()
//
package reader

import (
	"bytes"
	"io"
	"io/ioutil"
)

// An Opener has the facility to open a reader for a blob with a given id.  The
// reader only requires this portion of the blobstore interface.
type Opener interface {
	Open(id string) (io.ReadCloser, error)
}

// Concat returns an io.ReadCloser that produces the data of each of the blobs
// identified by ids, concatenated in the given sequence.  The returned reader
// should be closed when no longer needed.
func Concat(o Opener, ids []string) io.ReadCloser {
	return &blobRC{
		cur: ioutil.NopCloser(new(io.LimitedReader)), // sentinel
		ids: ids,
		o:   o,
	}
}

// ReadAll reads the complete contents of all the blobs identified, and returns
// the result as a slice of bytes.
func ReadAll(o Opener, ids []string) ([]byte, error) {
	r := Concat(o, ids)
	defer r.Close()
	var buf bytes.Buffer
	if _, err := io.Copy(&buf, r); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

type blobRC struct {
	cur io.ReadCloser // the current blob being consumed
	ids []string      // blob IDs remaining to be opened
	o   Opener        // the store containing the blobs

	// Invariant: cur != nil as long as Close has not completed.
}

func (b *blobRC) nextBlob() error {
	if b.cur != nil {
		b.cur.Close() // it doesn't matter if this fails
	}
	if len(b.ids) == 0 {
		return io.EOF // no more blobs to consider
	}
	r, err := b.o.Open(b.ids[0])
	if err != nil {
		return err
	}
	b.ids = b.ids[1:]
	b.cur = r
	return nil
}

// Read fetches data from a sequence of blob IDs in a Store, satisfying
// io.Reader.  The contents of the blobs are concatenated in order.
func (b *blobRC) Read(data []byte) (int, error) {
	nr, err := b.cur.Read(data)
	for err == io.EOF {
		if nerr := b.nextBlob(); nerr != nil {
			return nr, nerr
		}
		var m int
		m, err = b.cur.Read(data)
		nr += m
	}
	return nr, err
}

// Close cleans up any open files associated with the reader.  A call to Read
// after a successful Close will panic.
func (b *blobRC) Close() error {
	if c := b.cur; c != nil {
		b.cur = nil
		b.ids = nil
		return c.Close()
	}
	return nil
}
