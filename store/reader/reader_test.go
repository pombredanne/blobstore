package reader

import (
	"bytes"
	"errors"
	"io"
	"io/ioutil"
	"sort"
	"strconv"
	"strings"
	"testing"
)

type fake map[string]string

func (f fake) Open(id string) (io.ReadCloser, error) {
	v, ok := f[id]
	if !ok {
		return nil, errors.New("no such blob")
	}
	return ioutil.NopCloser(strings.NewReader(v)), nil
}

func TestReader(t *testing.T) {
	f := fake{
		"0": "",
		"1": "one",
		"2": "two",
		"3": "three",
		"4": "four",
		"5": "five",
		"6": "six",
	}

	tests := []struct {
		ids  []string
		want string
	}{
		{nil, ""},
		{[]string{}, ""},
		{[]string{"1", "1", "1"}, "oneoneone"},
		{[]string{"6", "1", "2", "3"}, "sixonetwothree"},
		{[]string{"1", "2", "4", "5", "1"}, "onetwofourfiveone"},
		{[]string{"1", "0", "6"}, "onesix"},
		{[]string{"1", "0", "0", "0", "0", "6"}, "onesix"},
	}

	for _, test := range tests {
		raw, err := ioutil.ReadAll(Concat(f, test.ids))
		if err != nil {
			t.Fatalf("Error reading %+q: %v", test.ids, err)
		}
		if got := string(raw); got != test.want {
			t.Errorf("Reading %+q: got %q, want %q", test.ids, got, test.want)
		} else {
			t.Logf("Read %q OK", got)
		}
	}
}

func TestBigReads(t *testing.T) {
	f := make(fake)
	v := make([]byte, 512)
	for i := range v {
		v[i] = byte(i % 256)
	}

	// Just generate a bunch of strings with diverse content.
	var keys []string
	for i := 0; i < 256; i++ {
		k := strconv.Itoa(i + 1)
		s := string(v[i : 2*i+1])
		f[k] = s
		keys = append(keys, k)
	}

	// A lexicographic permutation is as good as anything...
	sort.Strings(keys)
	var all bytes.Buffer
	for _, k := range keys {
		all.WriteString(f[k])
	}

	var out bytes.Buffer
	nr, err := io.Copy(&out, Concat(f, keys))
	if err != nil {
		t.Errorf("Error reading: %v\nKeys: %v", err, keys)
	}
	t.Logf("Copied %d of %d expected bytes from the blob reader", nr, all.Len())
	if got, want := out.String(), all.String(); got != want {
		t.Errorf("Incorrect read:\n got: %q\nwant: %q", got, want)
	}
}

func TestReaderErrors(t *testing.T) {
	f := fake{"a": "alpha", "b": "bravo"}
	tests := []struct {
		ids  []string
		want string
	}{
		{[]string{"c"}, ""},
		{[]string{"a", "b", "c"}, "alphabravo"},
		{[]string{"a", "c", "b"}, "alpha"},
		{[]string{"b", "b", "c", "c"}, "bravobravo"},
	}
	for _, test := range tests {
		raw, err := ioutil.ReadAll(Concat(f, test.ids))
		if err == nil {
			t.Errorf("Reading %+q: did not get the expected error", test.ids)
		}
		if got := string(raw); got != test.want {
			t.Errorf("Reading %+q: got %q, want %q", test.ids, got, test.want)
		} else {
			t.Logf("Read %q, err=%v", got, err)
		}
	}
}
