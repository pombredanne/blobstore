// Package storage describes an interface for storage capabilities needed to
// implement a blobstore, and provides on-disk and in-memory implementations of
// that interface.
package storage

import "io"

// Interface represents the storage capabilities required to implement a Store.
// A value of this type must be safe for concurrent access by multiple
// goroutines, though the values returned by its methods need not be.
type Interface interface {
	// Open returns a reader for an existing blob ID.  Returns an error that
	// satisfies os.IsNotExist if the blob does not exist.
	//
	// The caller is responsible for closing the reader when no longer in use.
	Open(id string) (io.ReadCloser, error)

	// Buffer returns a writer for a freshly-created temporary buffer.  Returns
	// an error if none could be created.  The caller must call either Commit
	// or Cancel on the buffer when finished to release its resources.
	Buffer() (Buffer, error)

	// Remove atomically deletes the specified blob ID, returning nil if and
	// only if the blob existed and was successfully removed.
	//
	// This method is optional, and a correct implementation may return an
	// error for all calls.
	Remove(id string) error

	// Keys calls f for each of the existing blob IDs known to the VFS, in
	// lexicographic order.  If f returns an error, Keys terminates and returns
	// the error returned by f unless it is io.EOF, in which case Keys will
	// return nil.
	Keys(f func(string) error) error
}

// A Buffer represents a buffer where the contents of a blob can be written.
type Buffer interface {
	io.Writer

	// Commit causes all the data written to the buffer to be stored under the
	// given ID. If a blob with that ID already exists, it is replaced without
	// error.
	Commit(id string) error

	// Cancel causes the buffered contents to be discarded.  Further writes on
	// the buffer will be ignored.
	Cancel()
}
