package storage

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"sort"
	"strings"
	"sync"
)

// Memory returns an in-memory implementation of storage.Interface.
// The concrete type of the value is *Mem.
func Memory() Interface { return &Mem{Blobs: make(map[string]string)} }

// Mem implements storage.Interface.  It is exported to support testing; for
// conventional use, use Memory to allocate a new value of this type.
type Mem struct {
	sync.Mutex
	Blobs map[string]string // exported to support testing
}

func (m *Mem) String() string { return fmt.Sprintf("#<memory-storage:%p>", m) }

func (m *Mem) Open(id string) (io.ReadCloser, error) {
	m.Lock()
	defer m.Unlock()
	if s, ok := m.Blobs[id]; ok {
		return ioutil.NopCloser(strings.NewReader(s)), nil
	}
	return nil, os.ErrNotExist
}

type memBlob struct {
	*bytes.Buffer
	m *Mem
}

func (m memBlob) Commit(id string) error {
	m.m.Lock()
	m.m.Blobs[id] = m.Buffer.String()
	m.m.Unlock()
	return nil
}

func (m memBlob) Cancel() {}

type memWriter struct {
	io.Writer
	key int
}

func (m *Mem) Buffer() (Buffer, error) { return memBlob{bytes.NewBuffer(nil), m}, nil }

func (m *Mem) Remove(id string) error {
	m.Lock()
	defer m.Unlock()
	if _, ok := m.Blobs[id]; ok {
		delete(m.Blobs, id)
		return nil
	}
	return fmt.Errorf("invalid ID %q", id)
}

func (m *Mem) Keys(f func(string) error) error {
	m.Lock()
	var keys []string
	for id := range m.Blobs {
		keys = append(keys, id)
	}
	m.Unlock()

	sort.Strings(keys)
	for _, id := range keys {
		if err := f(id); err == io.EOF {
			return nil
		} else if err != nil {
			return err
		}
	}
	return nil
}
