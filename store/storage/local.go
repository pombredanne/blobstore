package storage

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"sort"

	"golang.org/x/sys/unix"

	"github.com/pborman/uuid"
)

// Local returns a storage.Interface that uses files under the specified root
// directory for storage. Files and directories are manipulated via the
// functions in the os package.  The directory is created if it does not exist.
//
// This implementation uses a directory with the following structure:
//
//   root/
//      <xx>/
//        <tail>
//        ...
//      ...
//
// Each blob is stored in a file named by the id assigned by the writer, with
// the restriction that each ID must be at least three bytes long.  The first
// two bytes are used as a directory prefix; the rest are used as a filename.
// For example, the id "a1b2c3d4" will be stored as "root/a1/b2c3d4".
func Local(root string, mode os.FileMode) (Interface, error) {
	if err := os.MkdirAll(root, mode); err != nil {
		return nil, err
	}
	return local{root}, nil
}

type fileBuffer struct {
	*os.File
	root string
}

// Commit implements the corresponding method of storage.Buffer.
// It closes the temporary file and renames it to the target blob path.
func (f fileBuffer) Commit(id string) error {
	if err := f.File.Close(); err != nil {
		os.Remove(f.File.Name()) // best-effort to clean up the temp file.
		return err
	}
	dir := filepath.Join(f.root, id[:2])
	path := filepath.Join(dir, id[2:])
	if err := os.MkdirAll(dir, 0755); err != nil {
		return err
	}
	return os.Rename(f.Name(), path)
}

// Cancel implements the corresponding method of storage.Interface.
// It closes and discards the temporary file owned by the buffer.
func (f fileBuffer) Cancel() {
	f.Close()
	os.Remove(f.Name())
}

type local struct{ root string }

func (fs local) String() string { return fmt.Sprintf("#<local-storage:%q>", fs.root) }

func (fs local) blobPath(id string) string {
	if len(id) < 3 {
		return ""
	}
	return filepath.Join(fs.root, id[:2], id[2:])
}

func (fs local) Open(id string) (io.ReadCloser, error) {
	if path := fs.blobPath(id); path != "" {
		return os.Open(path)
	}
	return nil, fmt.Errorf("invalid ID %q", id)
}

// Buffer implements the corresponding method of storage.Interface.
// Writes to the buffer are stored in a temporary file in the storage directory.
func (fs local) Buffer() (Buffer, error) {
	path := filepath.Join(fs.root, uuid.New()+".tmp")
	f, err := os.Create(path)
	if err != nil {
		return nil, err
	}
	return fileBuffer{f, fs.root}, nil
}

// Remove implements the corresponding method of storage.Interface.
// It unlinks the file associated with id, if it exists.
func (fs local) Remove(id string) error {
	if path := fs.blobPath(id); path != "" {
		defer os.Remove(filepath.Dir(path)) // Clean up directory, if it's empty
		return os.Remove(path)
	}
	return fmt.Errorf("invalid ID %q", id)
}

// Keys implements the corresponding method of storage.Interface.
// It finds all known blob IDs by listing the contents of the storage
// directory, and sorting them in lexicographic order.
func (fs local) Keys(f func(string) error) error {
	pfx, err := fs.listdir(fs.root)
	if err != nil && !isNotDir(err) {
		return err
	}
	sort.Strings(pfx)
	for _, p := range pfx {
		if len(p) != 2 { // skip temp files, other
			continue
		}
		tails, err := fs.listdir(filepath.Join(fs.root, p))
		if err != nil && !isNotDir(err) {
			return err
		}
		sort.Strings(tails)
		for _, tail := range tails {
			if err := f(p + tail); err == io.EOF {
				return nil
			} else if err != nil {
				return err
			}
		}
	}
	return nil
}

func (fs local) listdir(dir string) ([]string, error) {
	f, err := os.Open(dir)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	return f.Readdirnames(-1)
}

func isNotDir(err error) bool {
	if se, ok := err.(*os.SyscallError); ok {
		return se.Err == unix.ENOTDIR
	}
	return false
}
