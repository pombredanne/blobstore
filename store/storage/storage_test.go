package storage

import (
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"path/filepath"
	"reflect"
	"sort"
	"strings"
	"sync"
	"testing"
)

var (
	tempDir = flag.String("dir", "", "Directory to use for test storage")
	cleanup = false
	once    sync.Once
)

func localStore(name string) (Interface, func()) {
	once.Do(func() {
		if *tempDir == "" {
			dir, err := ioutil.TempDir("", "storage-test")
			if err != nil {
				log.Panicf("Unable to create temp directory: %v", err)
			}
			*tempDir = dir
			cleanup = true
		}
		log.Printf("Test output will be stored in %q", *tempDir)
	})

	path := filepath.Join(*tempDir, name)
	s, err := Local(path, 0755)
	if err != nil {
		log.Panicf("Unable to create local storage at %q: %v", path, err)
	}
	if cleanup {
		return s, func() { os.RemoveAll(path) }
	}
	return s, func() {}
}

func taskErr(op, id string, err error) error {
	return fmt.Errorf("Task %s %s failed: %v", id, op, err)
}

func TestRoundTrip(t *testing.T) {
	const (
		input        = randString("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789./=")
		numBlobs     = 500
		maxSize      = 10000
		maxOpenFiles = 256
	)

	// Allocate a local (on-disk) store and an in-memory store.
	// The test will write the same blobs to both stores, and verify that the
	// results are the same in both.
	st, cleanup := localStore("RoundTrip")
	defer cleanup()
	stores := []Interface{st, Memory()}

	// Write concurrently into all storage buckets, to give the race detector
	// something to push against.
	ec := make(chan error)
	rl := make(chan struct{}, maxOpenFiles)
	var wg sync.WaitGroup
	var wantID []string
	wantData := make(map[string]string)

	for i := 0; i < numBlobs; i++ {
		in := input.random(maxSize)    // Write this string to each store.
		id := fmt.Sprintf("%04x", i+1) // Use this blob id.
		wantID = append(wantID, id)
		wantData[id] = in

		for _, st := range stores {
			wg.Add(1)
			go func(st Interface, id, in string) {
				rl <- struct{}{}
				defer func() { wg.Done(); <-rl }()

				r, w := io.Pipe()
				// Write in multiple chunks, to encourage the system to let the
				// goroutines interleave.
				go func() {
					for in != "" {
						n := rand.Intn(len(in)) + 1
						if _, err := fmt.Fprint(w, in[:n]); err != nil {
							w.CloseWithError(err)
							return
						}
						in = in[n:]
					}
					w.Close()
				}()
				if err := writeBlob(st, id, r); err != nil {
					ec <- err
				}
			}(st, id, in)
		}
	}
	go func() { wg.Wait(); close(ec) }()

	for err := range ec {
		t.Error(err)
	}

	// Verify that all storage buckets got the expected IDs and values.
	sort.Strings(wantID)
	for _, st := range stores {
		var gotID []string
		if err := st.Keys(func(id string) error {
			gotID = append(gotID, id)
			s, err := readString(st, id)
			if err != nil {
				t.Errorf("Error reading %q from %v: %v", id, st, err)
			} else if want := wantData[id]; s != want {
				t.Errorf("Mismatch for %q in %v: got %q, want %q", id, st, s, want)
			}
			return nil
		}); err != nil {
			t.Errorf("{%v}.Keys failed: %v", st, err)
		}
		if !reflect.DeepEqual(gotID, wantID) {
			t.Errorf("{%v}.Keys: got %+q, want %+q", st, gotID, wantID)
		}
	}

}

func TestConcurrency(t *testing.T) {
	// Test mixed concurrent reads and writes, for the race detector.
	const numWorkers = 20
	const numTasks = 500

	type task struct {
		store Interface
		write bool
		value string
	}
	ch := make(chan task)
	var wg sync.WaitGroup
	wg.Add(numWorkers)
	for i := 0; i < numWorkers; i++ {
		i := i
		go func() {
			defer wg.Done()
			for task := range ch {
				if task.write {
					if err := writeBlob(task.store, task.value, strings.NewReader(task.value)); err != nil {
						t.Errorf("Worker %d: %v", i, err)
					}
				} else if v, err := readString(task.store, task.value); err != nil {
					// This is OK; it might not have been written yet.
				} else if v != task.value {
					t.Errorf("Worker %d: read: got %q, want %q", i, v, task.value)
				}
			}
		}()
	}

	st, cleanup := localStore("Concurrency")
	defer cleanup()
	stores := []Interface{st, Memory()}
	keys := []string{"alpha", "bravo", "charlie", "delta", "echo", "foxtrot", "golf", "hotel"}

	for i := 0; i < numTasks; i++ {
		ch <- task{
			store: stores[rand.Intn(len(stores))],
			write: rand.Intn(2) != 0,
			value: keys[rand.Intn(len(keys))],
		}
	}
	close(ch)
	wg.Wait()
}

func readString(st Interface, id string) (string, error) {
	rc, err := st.Open(id)
	if err != nil {
		return "", err
	}
	defer rc.Close()
	data, err := ioutil.ReadAll(rc)
	if err != nil {
		return "", err
	}
	return string(data), nil
}

func writeBlob(st Interface, id string, r io.Reader) error {
	buf, err := st.Buffer()
	if err != nil {
		return taskErr("Buffer", id, err)
	} else if _, err := io.Copy(buf, r); err != nil {
		buf.Cancel()
		return taskErr("Copy", id, err)
	} else if err := buf.Commit(id); err != nil {
		return taskErr("Commit", id, err)
	}
	return nil
}

type randString string

func (r randString) random(n int) string {
	buf := make([]byte, n)
	for i := 0; i < n; i++ {
		buf[i] = r[rand.Intn(len(r))]
	}
	return string(buf)
}
