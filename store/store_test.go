package store

import (
	"compress/zlib"
	"crypto/sha1"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"reflect"
	"sort"
	"strings"
	"sync"
	"testing"

	"bitbucket.org/creachadair/blobstore/store/storage"
)

type zlibComp struct{}

func (zlibComp) Compress(w io.Writer) (io.WriteCloser, error)  { return zlib.NewWriter(w), nil }
func (zlibComp) Uncompress(r io.Reader) (io.ReadCloser, error) { return zlib.NewReader(r) }

func TestWriteBlobs(t *testing.T) {
	store := New(storage.Memory(), sha1.New)

	tests := []string{
		"O Captain! My Captain!",
		"Tiger, tiger, burning bright",
		"",
		"Tiger, tiger, burning bright", // Same blob again
	}
	ids := make(map[string]bool)
	for _, blob := range tests {
		b, err := store.New()
		if err != nil {
			t.Errorf("Failed to create blob for %q: %v", blob, err)
			continue
		}
		if _, err := b.Write([]byte(blob)); err != nil {
			t.Errorf("Failed write of %q: %v", blob, err)
			continue
		}
		id, err := b.Commit()
		if err != nil {
			t.Errorf("Commit failed for %q: %v", blob, err)
		}

		t.Logf("Wrote id %q for %q", id, blob)
		ids[id] = true
	}
	if len(ids) != 3 {
		t.Errorf("Unique blobs written: got %d, want %d", len(ids), 3)
	}
}

func TestReadBlobs(t *testing.T) {
	store := New(storage.Memory(), sha1.New)
	mem := store.Storage().(*storage.Mem)
	mem.Blobs = map[string]string{
		"A": "Alice, who fell down the stairs",
		"B": "Basil, assaulted by bears",
		"C": "Clara, who wasted away",
		"D": "Desmond, thrown out of a sleigh",
		"E": "Ernest, who choked on a peach",
		"F": "Fanny, sucked dry by a leech",
		"G": "George, smothered under a rug",
		"H": "Hector, done in by a thug",
	}

	err := store.Storage().Keys(func(id string) error {
		blob, err := store.Open(id)
		if err != nil {
			t.Errorf("Error opening blob %q: %v", id, err)
		} else if bits, err := ioutil.ReadAll(blob); err != nil {
			t.Errorf("Error reading blob %q: %v", id, err)
		} else {
			t.Logf("Read %d bytes for blob %q [%q]", len(bits), id, string(bits))
		}
		return nil
	})
	if err != nil {
		t.Errorf("Error scanning blobs: %v", err)
	}

	// While we're here, test Has.
	for _, c := range "ABCDEFGHIJKLMNOP" {
		key := string(c)
		_, want := mem.Blobs[key]
		if got := store.Has(key); got != want {
			t.Errorf("Has(%q): got %v, want %v", key, got, want)
		}
	}
}

func TestErrors(t *testing.T) {
	store := New(storage.Memory(), sha1.New)

	for _, bad := range []string{"", "0", "01", "012"} {
		if rc, err := store.Open(bad); err == nil {
			t.Errorf("Open %q unexpectedly succeeded: %v", bad, rc)
			rc.Close()
		} else {
			t.Logf("Open %q OK (got expected error): %v", bad, err)
		}
	}
}

func TestCopyReadAll(t *testing.T) {
	store := New(storage.Memory(), sha1.New)

	const testValue = "These are not the droids you're looking for.\n"

	id, err := store.Copy(strings.NewReader(testValue))
	if err != nil {
		t.Fatalf("Copy failed: %v")
	}
	t.Logf("Copy OK: %q", id)

	bits, err := store.ReadAll(id)
	if err != nil {
		t.Fatalf("ReadAll %q failed: %v", id, err)
	}
	if got := string(bits); got != testValue {
		t.Fatalf("Wrong value for %q: got %q, want %q", id, got, testValue)
	}
}

func TestListBlobs(t *testing.T) {
	store := New(storage.Memory(), sha1.New)

	var want []string
	for _, s := range []string{"alpha", "bravo", "charlie", "delta", "echo"} {
		id, err := store.Copy(strings.NewReader(s))
		if err != nil {
			t.Fatalf("Unable to write %q to the store: %v", s, err)
		}
		want = append(want, id)
	}
	sort.Strings(want)

	var ids []string
	if err := store.Storage().Keys(func(id string) error {
		ids = append(ids, id)
		return nil
	}); err != nil {
		t.Fatalf("Listing keys failed: %v", err)
	}
	if !reflect.DeepEqual(ids, want) {
		t.Errorf("Incorrect blob ID list:\n got: %+q\nwant: %+q", ids, want)
	}
}

func TestCompression(t *testing.T) {
	mem := storage.Memory().(*storage.Mem)
	store := New(mem, sha1.New, Compress(zlibComp{}))

	const wantSize = 17654

	input := strings.Repeat("a", wantSize)
	id, err := store.Copy(strings.NewReader(input))
	if err != nil {
		t.Fatalf("Error writing test blob: %v", err)
	}
	data, ok := mem.Blobs[id]
	if !ok {
		t.Errorf("Blob %q not found", id)
	} else if len(data) >= len(input) {
		t.Errorf("Compression not applied: size is %d, want ≤ %d", len(data), len(input))
	}
	t.Logf("Stored size of blob %q is %d bytes", id, len(data))
}

func TestConcurrency(t *testing.T) {
	store := New(storage.Memory(), sha1.New, Compress(zlibComp{}))

	const message = "abcdefghijklmnopqrstuvwxyz0123456789"
	const count = len(message) - 4

	var wg sync.WaitGroup
	wg.Add(count)
	var μ sync.Mutex
	ids := make(map[string]bool)
	for i := 0; i < count; i++ {
		go func(i int, s string) {
			defer wg.Done()
			msg := strings.Repeat(s, i+1)
			b, err := store.New()
			if err != nil {
				t.Errorf("Worker %d: new blob failed: %v", i, err)
				return
			}
			defer b.Cancel()

			if _, err := fmt.Fprintln(b, msg); err != nil {
				t.Errorf("Worker %d: write %q failed: %v", i, msg, err)
				return
			}

			id, err := b.Commit()
			if err != nil {
				t.Errorf("Worker %d: commit failed: %v", i, err)
				return
			}

			μ.Lock()
			ids[id] = true
			μ.Unlock()
		}(i, message[i:i+5])
	}
	wg.Wait()

	// Verify that all the blobs were successfully created.
	var gotCount int
	if err := store.Storage().Keys(func(id string) error {
		bits, err := store.ReadAll(id)
		if err != nil {
			t.Errorf("Unable to read blob %q: %v", id, err)
		} else {
			gotCount++
			t.Logf("Read %d bytes for blob %q", len(bits), id)
		}
		return nil
	}); err != nil {
		t.Errorf("Scanning blobs failed: %v", err)
	}
	if gotCount != len(ids) {
		t.Errorf("Read %d blobs, wanted %d blobs", gotCount, count)
	}
}

func ExampleWriteBlob() {
	store := New(storage.Memory(), sha1.New)

	b, err := store.New()
	if err != nil {
		log.Printf("Unable to create blob: %v", err)
		return
	}
	defer b.Cancel()
	if _, err := io.Copy(b, strings.NewReader("t'was brillig, and the slithy toves...\n")); err != nil {
		log.Printf("Unable to write blob: %v", err)
		return
	}
	if id, err := b.Commit(); err != nil {
		log.Printf("Unable to commit blob: %v", err)
	} else {
		fmt.Println("blob digest is", id)
	}
	// Output: blob digest is 4c710e10ca9a3690fd3765b3a3a076a65d03b278
}
