// Package s3storage implements the storage.Interface around an S3 (AWS) bucket.
package s3storage

import (
	"io"
	"io/ioutil"
	"os"
	"sort"
	"strings"

	"bitbucket.org/creachadair/blobstore/store/storage"

	"gopkg.in/amz.v2/s3"
)

// Bucket returns a storage.Interface that stores blobs in an S3 bucket under
// the specified prefix.
func Bucket(aws *s3.S3, bucketName, prefix string) storage.Interface {
	if prefix != "" && !strings.HasSuffix(prefix, "/") {
		prefix += "/"
	}
	return &s3bucket{
		bucket: aws.Bucket(bucketName),
		prefix: prefix,
	}
}

type s3bucket struct {
	bucket *s3.Bucket
	prefix string
}

func (b *s3bucket) blobPath(id string) string { return b.prefix + id }

// Open implements the corresponding method of storage.Interface.
func (b *s3bucket) Open(id string) (io.ReadCloser, error) { return b.bucket.GetReader(b.blobPath(id)) }

// Buffer implements the corresponding method of storage.Interface.
func (b *s3bucket) Buffer() (storage.Buffer, error) {
	f, err := ioutil.TempFile("", "s3blob")
	if err != nil {
		return nil, err
	}
	return s3blob{f, b}, nil
}

type s3blob struct {
	*os.File
	bucket *s3bucket
}

// Commit implements the corresponding method of storage.Buffer.
func (b s3blob) Commit(id string) error {
	defer os.Remove(b.File.Name()) // best-effort

	fi, err := b.File.Stat()
	if err != nil {
		return err
	}
	if _, err := b.File.Seek(0, 0); err != nil {
		return err
	}
	defer b.File.Close()
	return b.bucket.bucket.PutReader(b.bucket.blobPath(id), b.File, fi.Size(), "application/octet-stream", s3.Private)
}

// Cancel implements the corresponding method of storage.Interface.
func (b s3blob) Cancel() {
	b.File.Close()
	os.Remove(b.File.Name())
}

// Remove implements the corresponding method of storage.Interface.
//
// TODO: Right now DELETE does not return an error if the blob doesn't exist,
// but the s3 package doesn't expose the HEAD method to let us check.  Add
// that, then fix the semantics here.
func (b *s3bucket) Remove(id string) error { return b.bucket.Del(b.blobPath(id)) }

// Keys implements the corresponding method of storage.Interface.
func (b *s3bucket) Keys(f func(string) error) error {
	var keys []string
	var marker string

	hasMore := true // maybe
	for hasMore {
		more, err := b.bucket.List(b.prefix, "", marker, 1000)
		if err != nil {
			return err
		}
		for _, key := range more.Contents {
			keys = append(keys, strings.TrimPrefix(key.Key, b.prefix))
		}
		hasMore = more.IsTruncated
	}
	sort.Strings(keys)
	for i, key := range keys {
		if i == 0 || key != keys[i-1] {
			switch err := f(key); err {
			case nil:
				continue
			case io.EOF:
				return nil
			default:
				return err
			}
		}
	}
	return nil
}
