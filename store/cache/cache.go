// Package cache implements storage.Interface by delegating to another
// implementation of the same interface, through an in-memory cache.
package cache

import (
	"bytes"
	"io"
	"io/ioutil"

	"bitbucket.org/creachadair/blobstore/store/storage"
	"bitbucket.org/creachadair/cache/lru"
	"bitbucket.org/creachadair/cache/value"
)

// New returns an implementation of storage.Interface that can cache up to
// capacity bytes of blob data in memory, and delegates all storage operations
// through to st.
func New(st storage.Interface, capacity int) storage.Interface {
	return &cachedStorage{st: st, cache: lru.New(capacity)}
}

type cachedStorage struct {
	st    storage.Interface
	cache *lru.Cache
}

// Open implements the corresponding method of storage.Interface.  If the data
// are not already cached, and the caller consumes full contents of the reader,
// the resulting data will be added to the cache when the reader is closed if
// there is sufficient space in the cache.
func (c *cachedStorage) Open(id string) (io.ReadCloser, error) {
	if data := c.cache.Get(id); data != nil {
		bits := []byte(data.(value.Bytes))
		return ioutil.NopCloser(bytes.NewReader(bits)), nil
	}
	rc, err := c.st.Open(id)
	if err != nil {
		return nil, err
	}
	return &rpair{rc, bytes.NewBuffer(nil), c.cache, id}, nil
}

// rpair is an io.ReadCloser that mirrors reads from str into a memory buffer.
// When it is closed, the resulting copy will be added to the cache.
type rpair struct {
	str   io.ReadCloser
	buf   *bytes.Buffer
	cache *lru.Cache
	id    string
}

func (r *rpair) Read(data []byte) (int, error) {
	nr, err := r.str.Read(data)
	if nr > 0 && r.buf != nil {
		// If this write will generate more data than the cache can hold, there
		// is no point in keeping it in memory; discard the mirror blob.
		if r.buf.Len()+nr <= r.cache.Cap() {
			r.buf.Write(data[:nr])
		} else {
			r.buf = nil
		}
	}
	return nr, err
}

func (r *rpair) Close() error {
	if err := r.str.Close(); err != nil {
		return err
	}
	// If we still have a mirror blob at this point, push it into the cache.
	if r.buf != nil {
		r.cache.Put(r.id, value.Bytes(r.buf.Bytes()))
	}
	return nil
}

// wpair is an io.Writer that mirrors writes to a buffer as well as passing
// them to stw.  This allows a buffer to be cached without knowledge of its
// implementation, at the cost of an extra copy of the data.
type wpair struct {
	stw   storage.Buffer
	buf   *bytes.Buffer
	cache *lru.Cache
}

func (w *wpair) Write(data []byte) (int, error) {
	nw, err := w.stw.Write(data)
	if nw > 0 && w.buf != nil {
		if w.buf.Len()+nw <= w.cache.Cap() {
			w.buf.Write(data[:nw])
		} else {
			w.buf = nil
		}
	}
	return nw, err
}

// Commit implements the corresponding method of storage.Buffer.
// If committing to the underlying storage succeeds, the data are also written
// into the cache.
func (w *wpair) Commit(id string) error {
	if err := w.stw.Commit(id); err != nil {
		return err
	}
	// If we still have a mirror blob, put it into the cache.
	if w.buf != nil {
		w.cache.Put(id, value.Bytes(w.buf.Bytes()))
	}
	return nil
}

// Cancel implements the corresponding method of storage.Buffer.
func (w *wpair) Cancel() { w.stw.Cancel(); w.buf = nil }

// Buffer implements the corresponding method of storage.Interface.
// It allocates a buffer from the underlying storage, and returns a writer that
// copies writes both to that buffer and an in-memory buffer.
func (c *cachedStorage) Buffer() (storage.Buffer, error) {
	buf, err := c.st.Buffer()
	if err != nil {
		return nil, err
	}
	return &wpair{buf, bytes.NewBuffer(nil), c.cache}, nil
}

// Remove implements the corresponding method of storage.Interface.
// Whether or not removal succeeds from the underlying storage, the blob is
// dropped from the cache if it exists there.
func (c *cachedStorage) Remove(id string) error {
	c.cache.Drop(id)
	return c.st.Remove(id)
}

// Keys implements the corresponding method of storage.Interface.
// It delegates directly to the underlying storage, without touching the cache.
func (c *cachedStorage) Keys(f func(string) error) error { return c.st.Keys(f) }
