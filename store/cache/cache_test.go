package cache

import (
	"fmt"
	"io/ioutil"
	"reflect"
	"testing"

	"bitbucket.org/creachadair/blobstore/store/storage"
	"bitbucket.org/creachadair/cache/value"
)

func TestBasics(t *testing.T) {
	st := storage.Memory()
	cs := New(st, 16)
	cache := cs.(*cachedStorage).cache

	mustWrite := func(id, data string) {
		buf, err := cs.Buffer()
		if err != nil {
			t.Fatalf("Unable to allocate buffer: %v", err)
		}
		if _, err := fmt.Fprint(buf, data); err != nil {
			t.Fatalf("Unable to write %q: %v", data, err)
		}
		if err := buf.Commit(id); err != nil {
			t.Fatalf("Unable to commit %q: %v", id, err)
		}
	}
	isCached := func(id, val string) {
		if data := cache.Get(id); data == nil {
			t.Errorf("Missing id %q in cache", id)
		} else if got := string(data.(value.Bytes)); got != val {
			t.Errorf("Get %q: got %q, want %q", id, got, val)
		}
	}
	notCached := func(id string) {
		if data := cache.Get(id); data != nil {
			t.Errorf("Get %q: got %q, want nil", id, string(data.(value.Bytes)))
		}
	}
	isStored := func(id, value string) {
		if v, ok := st.(*storage.Mem).Blobs[id]; !ok {
			t.Errorf("Missing id %q in store", id)
		} else if v != value {
			t.Errorf("Lookup %q: got %q, want %q", id, v, value)
		}
	}

	notCached("a")
	mustWrite("a", "foobar")
	isStored("a", "foobar")
	isCached("a", "foobar")
	mustWrite("b", "bazquux")
	isStored("b", "bazquux")
	isCached("b", "bazquux")

	// If we overflow the cache, we evict the least-recently-used thing.
	mustWrite("c", "overflow") // evict "a"
	isCached("c", "overflow")
	isCached("b", "bazquux")
	notCached("a")
	isStored("c", "overflow")

	// If we write a blob that is too big for the cache, it doesn't get saved,
	// but still does survive writing.
	mustWrite("huge", "this is bigger than the capacity of the whole cache")
	isStored("huge", "this is bigger than the capacity of the whole cache")
	notCached("huge")

	// If we open and read a blob that is in the underlying store but not yet
	// cached, we should get it cached.
	st.(*storage.Mem).Blobs["d"] = "fetchable" // poke a blob into the store
	isStored("d", "fetchable")
	notCached("d")
	if rc, err := cs.Open("d"); err != nil {
		t.Errorf("Open d: unexpected error: %v", err)
	} else if _, err := ioutil.ReadAll(rc); err != nil {
		t.Errorf("Read d: unexpected error: %v", err)
	} else if err := rc.Close(); err != nil {
		t.Errorf("Close: unexpected error: %v", err)
	}
	isCached("d", "fetchable")

	// If we delete a blob from the store, it also goes from the cache.
	isStored("b", "bazquux")
	isCached("b", "bazquux")
	if err := cs.Remove("b"); err != nil {
		t.Errorf("Remove b: unexpected error: %v", err)
	}
	notCached("b")

	// Listing the cache keys works as designed.
	var keys []string
	if err := cs.Keys(func(s string) error {
		keys = append(keys, s)
		return nil
	}); err != nil {
		t.Errorf("Keys: unexpected error: %v", err)
	}
	want := []string{"a", "c", "d", "huge"} // these must be ordered
	if !reflect.DeepEqual(keys, want) {
		t.Errorf("Keys:\n got %+q\nwant %+q", keys, want)
	}
}
