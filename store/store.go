// Package store implements a content-addressable collection of data blobs.
//
// It is safe for multiple concurrent goroutines and processes to read and
// write blobs in a Store, but it is not safe for blobs to be deleted from the
// store while there are concurrent writers.
package store

import (
	"encoding/hex"
	"hash"
	"io"
	"io/ioutil"

	"bitbucket.org/creachadair/blobstore/store/storage"
)

// A Store represents a content-addressable blob store.
type Store struct {
	st     storage.Interface // Where the data are actually written
	digest func() hash.Hash  // Used to generate blob digests
	comp   Compressor        // If non-nil, used to compress/uncompress blob data
}

// A Compressor expresses the ability to compress and uncompress data.
type Compressor interface {
	// Return an io.WriteCloser that compresses the data written to it and
	// writes it to w.
	Compress(w io.Writer) (io.WriteCloser, error)

	// Return an io.ReadCloser that reads data from r and uncompresses it.
	Uncompress(r io.Reader) (io.ReadCloser, error)
}

// An option represents a configuration option for a Store.
type Option func(*Store)

// Compress is an option that enables (if c != nil) or disables (if c == nil)
// compression of blob data.
func Compress(c Compressor) Option { return func(s *Store) { s.comp = c } }

// New creates a new store associated with the given storage.Interface.  Each
// blob is identified by a cryptographic digest of its (uncompressed) data,
// encoded as lower-case hex digits.
//
// A *Store is safe for concurrent reading and writing by multiple goroutines
// once initialized.  New always returns an initialized *Store, or nil.
//
// New will panic if st == nil or digest == nil.
func New(st storage.Interface, digest func() hash.Hash, opts ...Option) *Store {
	if st == nil {
		panic("storage interface is nil")
	} else if digest == nil {
		panic("digest function is nil")
	}
	s := &Store{
		st:     st,
		digest: digest,
	}
	for _, opt := range opts {
		opt(s)
	}
	return s
}

// Storage returns the storage interface for the store.
func (s *Store) Storage() storage.Interface { return s.st }

type nopCloser struct{ io.Writer }

func (nopCloser) Close() error { return nil }

// New creates a new empty blob in the store.  A newly-created blob is not
// committed into the store until its Commit method is called.
func (s *Store) New() (*Blob, error) {
	buf, err := s.st.Buffer()
	if err != nil {
		return nil, err
	}
	var w io.WriteCloser

	// If compression is enabled, add in a compressor.
	if s.comp == nil {
		w = nopCloser{buf}
	} else {
		c, err := s.comp.Compress(buf)
		if err != nil {
			return nil, err
		}
		w = c
	}
	return &Blob{
		buf: buf, // always the original storage buffer
		w:   w,   // may be buf, or may have compression (write here)
		h:   s.digest(),
		s:   s,
	}, nil
}

// Copy creates a blob by copying the data from r, and commits the blob.
// Returns the id of the resulting blob on success.
func (s *Store) Copy(r io.Reader) (string, error) {
	b, err := s.New()
	if err != nil {
		return "", err
	}
	if _, err := io.Copy(b, r); err != nil {
		b.Cancel()
		return "", err
	}
	return b.Commit()
}

// Open returns a reader positioned at the beginning of the blob whose id is
// given, if it exists.  The caller must close the reader when finished.
func (s *Store) Open(id string) (io.ReadCloser, error) {
	r, err := s.st.Open(id)
	if err != nil {
		return nil, err
	}
	if s.comp != nil {
		c, err := s.comp.Uncompress(r)
		if err != nil {
			return nil, err
		}
		// Compressor implementations cannot close their reader, so we have to
		// make sure that happens to avoid file descriptor exhaustion.
		return &rcChain{c, r}, nil
	}
	return r, nil
}

// Has reports whether a blob with the given id exists in the store.  This
// returns false if id is not a valid blob ID.
func (s *Store) Has(id string) bool {
	if r, err := s.st.Open(id); err == nil {
		r.Close()
		return true
	}
	return false
}

// ReadAll opens and completely reads the blob whose id is given, if it exists.
func (s *Store) ReadAll(id string) ([]byte, error) {
	v, err := s.Open(id)
	if err != nil {
		return nil, err
	}
	defer v.Close()
	return ioutil.ReadAll(v)
}

// A Blob represents a writable blob of data.  Add data to the blob using its
// Write method and call Commit to commit the blob to the store.  A given blob
// is not safe for concurrent access by multiple goroutines.
//
// Typical workflow:
//   b, err := st.New()
//   if err != nil {
//      return err
//   }
//   defer b.Cancel()
//   if _, err := io.Copy(b, someReader); err != nil {
//      return err // the cancel will take effect
//   }
//   return b.Commit() // if this succeeds, the cancel will be a no-op
//
type Blob struct {
	buf storage.Buffer // the original buffer from the storage interface
	w   io.WriteCloser // includes buffering and compression, if set
	h   hash.Hash
	s   *Store
}

// Write adds data to the blob, satisfying io.Writer.
func (b *Blob) Write(data []byte) (int, error) {
	nw, err := b.w.Write(data)
	if nw > 0 {
		b.h.Write(data[:nw])
	}
	return nw, err
}

// Commit commits the blob to the store and returns its id.
func (b *Blob) Commit() (string, error) {
	if err := b.w.Close(); err != nil {
		return "", err
	}
	id := hex.EncodeToString(b.h.Sum(nil))
	if err := b.buf.Commit(id); err != nil {
		return "", err
	}
	b.w = nil
	b.buf = nil
	return id, nil
}

// Cancel discards the blob and cleans up its state.  It is safe to Cancel a
// blob after its Commit method has succeeded; such a Cancel is a no-op.
func (b *Blob) Cancel() {
	if b.w != nil {
		// We don't care whether this operation fails, it is just a good-faith
		// effort to avoid leaking resources.
		b.buf.Cancel()
		b.w = nil
		b.buf = nil
	}
}

// rcChain implements io.ReadCloser for a chain of io.ReadCloser values that
// need to be closed in succession.
type rcChain []io.ReadCloser

func (r rcChain) Read(data []byte) (int, error) { return r[0].Read(data) }

func (r rcChain) Close() error {
	var err error
	for _, rc := range r {
		if e := rc.Close(); err == nil {
			err = e
		}
	}
	return err
}
