package tree

import (
	"bytes"
	"errors"
	"fmt"
	"hash/adler32"
	"io"
	"io/ioutil"
	"reflect"
	"strings"
	"testing"
)

func hashString(s string) string { return fmt.Sprintf("%08x", adler32.Checksum([]byte(s))) }

type dummyStore map[string]string

func newStore(blobs ...string) dummyStore {
	d := make(dummyStore)
	for _, blob := range blobs {
		d[hashString(blob)] = blob
	}
	return d
}

func (d dummyStore) Open(id string) (io.ReadCloser, error) {
	if strings.HasPrefix(id, "*") {
		return nil, errors.New(strings.TrimLeft(id, "*"))
	}
	data, ok := d[id]
	if !ok {
		return nil, errors.New("no such blob")
	}
	return ioutil.NopCloser(strings.NewReader(data)), nil
}

func (d dummyStore) Copy(r io.Reader) (string, error) {
	data, err := ioutil.ReadAll(r)
	if err != nil {
		return "", err
	}
	value := string(data)
	id := hashString(value)
	d[id] = value
	return id, nil
}

func TestOpen(t *testing.T) {
	const testData = `node 10
abcd efgh
once upon a daydream`
	wantKids := []string{"ABCD", "EFGH"}
	st := newStore(testData)
	key := hashString(testData)

	n, err := Open(st, key)
	if err != nil {
		t.Fatalf("Error fetching %q: %v", key, err)
	}
	var buf bytes.Buffer
	if _, err := io.Copy(&buf, n); err != nil {
		t.Errorf("Error copying data: %v", err)
	}

	if n.ID != key {
		t.Errorf("Node ID: got %q, want %q", n.ID, key)
	}
	if reflect.DeepEqual(n.Kids, wantKids) {
		t.Errorf("Kids: got %+q, want %+q", n.Kids, wantKids)
	}
	if got, want := buf.String(), "once upon a daydream"; got != want {
		t.Errorf("Data: got %q, want %q", got, want)
	}
	if err := n.Close(); err != nil {
		t.Errorf("Close error: %v", err)
	}
}

func TestOpenErrors(t *testing.T) {
	d := dummyStore{
		"empty":           "",
		"bogus header 1":  "xyzzy",
		"bogus header 2":  "45\n",
		"short header":    "node ",
		"bad length 1":    "node pdq\n",
		"bad length 2":    "node -3210\n",
		"short block":     "node 25\n123",
		"bogus block":     "node 5\n abcd",
		"o'erlong header": "node 8\nabcde fghij\n",
	}
	for key := range d {
		if n, err := Open(d, key); err == nil {
			t.Errorf("Open(_, %q): got %+v, wanted error", key, n)
		} else {
			t.Logf("Open(_, %q): got expected error:\n\t%v", key, err)
		}
	}
	if id, err := Open(d, "not here"); err == nil {
		t.Errorf("Open(_, absent): got %q, wanted error", id)
	} else {
		t.Logf("Open(_, absent): got expected error:\n\t%v", err)
	}
}

func TestRoundTrip(t *testing.T) {
	d := make(dummyStore)
	tests := []struct {
		kids []string
		data string
	}{
		{},
		{data: "this is all data"},
		{[]string{"deedle", "deedle", "dee"}, "wubba wubba wubba\n"},
	}

	for _, test := range tests {
		id, err := New(d, test.kids, strings.NewReader(test.data))
		if err != nil {
			t.Errorf("New %+v failed: %v", test, err)
			continue
		}
		t.Logf("Wrote %+q as %s [%q]", test, id, d[id])

		n, err := Open(d, id)
		if err != nil {
			t.Errorf("Open(_, %q) failed: %v\ndata: %q", id, err, d[id])
			continue
		}
		var buf bytes.Buffer
		if _, err := io.Copy(&buf, n); err != nil {
			t.Errorf("Error copying data: %v", err)
		}

		if n.ID != id {
			t.Errorf("Node ID: got %q, want %q", n.ID, id)
		}
		if !reflect.DeepEqual(n.Kids, test.kids) {
			t.Errorf("Kids: got %+q, want %+q", n.Kids, test.kids)
		}
		if got := buf.String(); got != test.data {
			t.Errorf("Data: got %q, want %q", got, test.data)
		}
		if err := n.Close(); err != nil {
			t.Errorf("Close error: %v", err)
		}
	}
}

func TestReadNode(t *testing.T) {
	d := dummyStore{"q": `node 4
a b
happy data
are best data`,
	}
	wantKids := []string{"a", "b"}
	const wantData = "happy data\nare best data"

	n, data, err := ReadNode(d, "q")
	if err != nil {
		t.Fatalf(`ReadNode("q"): unexpected error: %v`, err)
	}
	if n.ID != "q" {
		t.Errorf(`Node ID: got %q, want "q"`, n.ID)
	}
	if !reflect.DeepEqual(n.Kids, wantKids) {
		t.Errorf("Kids: got %+q, want %+q", n.Kids, wantKids)
	}
	if got := string(data); got != wantData {
		t.Errorf("Data: got %q, want %q", got, wantData)
	}
}
