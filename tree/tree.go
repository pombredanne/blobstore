// Package tree provides a wrapper that encodes tree-structured data in a blob
// store.  A *tree.Node is a specially-encoded blob that may contain an ordered
// sequence of pointers to other blobs in addition to having data of its own.
//
// A tree blob begins with a space-delimited line having the following fields:
//
//   node <psize>
//
// where "node" is a literal string and psize is a non-negative integer encoded
// as ASCII decimal digits.
//
// Immediately following the header are psize bytes of pointer data. Any data
// following the pointer data are considered unstructured metadata.  It is
// legal for both the pointer data and metadata to be empty, so a minimal tree
// blob is simply a single line "tree 0\n".  A tree blob is invalid if its size
// in bytes is less than len(header) + psize.
//
// Pointer data are encoded as a space-delimited line containing hex-encoded
// digest strings.
//
// A space-delimited line is a sequence of fields, each consisting of one or
// more printable non-control ASCII characters, ending in a newline (NL).  Each
// field is separated from its immediate predecessor by a single space (SP).
//
// A hex-encoded string is a non-empty sequence of hexadecimal digits encoded
// in ASCII, having even length.
//
//
package tree

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"strconv"
	"strings"
)

const (
	// maxHeader is the maximum valid length for a header line.
	maxHeader = 32

	// maxPointer is the maximum valid length for a pointer ID.
	maxPointer = 256
)

// A Node represents a tree-structured blob.  A *Node returned by the Open
// function satisfies the io.ReadCloser interface, positioned at the beginning
// of the ancillary data stored at the end of the blob.
type Node struct {
	ID   string   // The blob ID of this node
	Kids []string // Pointers to other blobs (blob IDs)

	data *bufio.Reader // Reader for metadata
	c    io.Closer     // Closer for metadata
}

// Read satisfies io.Reader, reading data from the metadata block of n.
func (n *Node) Read(data []byte) (int, error) { return n.data.Read(data) }

// Close satisfies io.Closer, closing the underlying blob reader for n.
func (n *Node) Close() error { return n.c.Close() }

// New creates and stores a node pointing to the specified kids.
// If r != nil, its contents will be fully read into the metadata block.
func New(c Copier, kids []string, r io.Reader) (string, error) {
	var buf bytes.Buffer
	if ptr := strings.Join(kids, " "); ptr != "" {
		fmt.Fprintf(&buf, "node %d\n", len(ptr)+1)
		fmt.Fprintln(&buf, ptr)
	} else {
		fmt.Fprintln(&buf, "node 0")
	}
	if r == nil {
		return c.Copy(&buf)
	}
	return c.Copy(io.MultiReader(&buf, r))
}

// Open reads and returns the node stored under the given blob ID.  The caller
// is responsible for closing the node when it is no longer needed.
func Open(o Opener, id string) (node *Node, err error) {
	blob, err := o.Open(id)
	if err != nil {
		return nil, fmt.Errorf("tree: node not found for %q: %v", id, err)
	}
	defer func() {
		if node == nil {
			blob.Close()
		}
	}()

	// Read and verify the structure of the header.
	buf := bufio.NewReader(blob)
	hdr, err := scanFor(buf, "\n", maxHeader)
	if err != nil {
		return nil, fmt.Errorf("tree: missing header: %v", err)
	}
	fields := strings.Split(hdr[:len(hdr)-1], " ")
	if len(fields) != 2 || fields[0] != "node" {
		return nil, fmt.Errorf("tree: invalid header: %q", fields)
	}

	// Parse the pointer block size and read its data (if any).
	var kids []string
	size, err := strconv.Atoi(fields[1])
	if err != nil || size < 0 {
		return nil, fmt.Errorf("tree: invalid header length %q", fields[1])
	}
	i := 0
	for i < size {
		ptr, err := scanFor(buf, " \n", maxPointer)
		if err != nil {
			return nil, fmt.Errorf("tree: invalid pointer block: %v", err)
		}
		kids = append(kids, ptr[:len(ptr)-1])
		i += len(ptr)
		if ptr[len(ptr)-1] == '\n' {
			break
		}
	}
	if i != size { // Require that the pointer block was exactly consumed
		return nil, fmt.Errorf("tree: invalid length: %v", err)
	}

	// Assemble and return the node.
	return &Node{ID: id, Kids: kids, data: buf, c: blob}, nil
}

// ReadNode opens and fully reads the contents of the specified node.  The
// returned node is already closed.
func ReadNode(o Opener, id string) (*Node, []byte, error) {
	n, err := Open(o, id)
	if err != nil {
		return nil, nil, err
	}
	bits, err := ioutil.ReadAll(n)
	if err != nil {
		return nil, nil, err
	}
	return n, bits, nil
}

// scanFor returns a prefix of buf ending with any element of delim, having
// total length at most max.  Returns an error if no delimiter is found within
// that range.  The delimiter is the last byte of the string returned.
func scanFor(buf *bufio.Reader, delim string, max int) (string, error) {
	ln := make([]byte, max)
	for i := 0; i < max; i++ {
		if c, err := buf.ReadByte(); err != nil {
			return "", err
		} else if strings.IndexByte(delim, c) >= 0 {
			return string(ln[:i+1]), nil
		} else {
			ln[i] = c
		}
	}
	return "", errors.New("missing delimiter")
}

// A Copier writes the contents of r to a blobstore and returns the blob ID for
// the stored data.
type Copier interface {
	Copy(r io.Reader) (string, error)
}

// An Opener opens a specified blob and returns a reader for it.
type Opener interface {
	Open(id string) (io.ReadCloser, error)
}
